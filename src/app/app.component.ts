import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { ShapeService } from 'src/app/shape.service';
import { InAppNotificationService } from 'src/app/in-app-notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  fitScreen = false;
  imageXml: string;
  query = '';

  constructor(
    private shapeService: ShapeService,
    private notificationService: InAppNotificationService,
  ) { }

  onSubmit() {
    this.imageXml = undefined;

    this.shapeService.getShape(this.query).subscribe(xml => {
      this.notificationService.open('Shape generated');
      this.imageXml = xml;
    }, (error) => {
      this.notificationService.showGenericError();
    });
  }

  toggleFitScreen() {
    this.fitScreen = !this.fitScreen;
  }
}
