import { TestBed, inject } from '@angular/core/testing';

import { InAppNotificationService } from './in-app-notification.service';

describe('InAppNotificationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InAppNotificationService]
    });
  });

  it('should be created', inject([InAppNotificationService], (service: InAppNotificationService) => {
    expect(service).toBeTruthy();
  }));
});
