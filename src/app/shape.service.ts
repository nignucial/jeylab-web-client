import { Injectable, Sanitizer, SecurityContext } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ShapeService {

  constructor(
    private http: HttpClient,
    private sanitizer: DomSanitizer,
  ) { }

  getShape(query: string): Observable<string> {
    return this.http.get(environment.backend.urlBase + environment.backend.shapesPath, {
      params: new HttpParams({
        fromObject: { query: query }
      }),
      responseType: 'text',
    }).pipe(
      map(xml => this.sanitizer.bypassSecurityTrustHtml(xml) as string)
    );
  }

}
