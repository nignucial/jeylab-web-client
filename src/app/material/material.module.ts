import { NgModule } from '@angular/core';
import { MatCardModule, MatButtonModule, MatInputModule, MatSnackBarModule } from '@angular/material';

@NgModule({
  imports: [
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatSnackBarModule,
  ],
  exports: [
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatSnackBarModule,
  ]
})
export class MaterialModule { }
