import { Injectable } from '@angular/core';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class InAppNotificationService {

  private readonly config: MatSnackBarConfig = { duration: 3200 };

  constructor(private matSnackBar: MatSnackBar) { }

  open(message: string, action?: string, config?: MatSnackBarConfig): void {
    this.matSnackBar.open(message, action, Object.assign(this.config, config));
  }

  showGenericError(action?: string, config?: MatSnackBarConfig) {
    this.open('An error occurred; please try again later.', action, config);
  }

}
