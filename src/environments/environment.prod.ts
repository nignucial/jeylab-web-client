export const environment = {
  production: true,
  backend: {
    urlBase: 'http://localhost:63421/api',
    shapesPath: '/Shapes'
  }
};
